from setuptools import setup, find_packages

setup(
    name             = "discord-base-bots",
    packages         = find_packages(exclude=["tests"]),
    version          = "0.1.19",
    description      = "What bots are made of",
    author           = "jferretti",
    author_email     = "jon@jonathanferretti.com",
    url              = "https://gitlab.com/LISTERINE/discord-base-bots",
    classifiers      = ["Development Status :: 4 - Beta"],
    python_requires  = ">=3.6",
    install_requires = ["discord.py", "python-dotenv", "idna==2.10"]
)

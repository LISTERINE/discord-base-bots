clean:
	rm -rf build dist *.egg-info

pypi:
	$(MAKE) clean
	python setup.py bdist_wheel
	twine upload dist/*
